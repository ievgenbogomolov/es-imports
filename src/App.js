import logo from './logo.svg';
// Wrong way (import the whole library)
// import { map } from 'lodash'
// Right way (import only specific module)
import map from 'lodash/map'
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Imports example</h1>
        {map([...Array(10).keys()], (item) => (<p key={item}>Item: {item}</p>))}
      </header>
    </div>
  );
}

export default App;
