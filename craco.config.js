const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const isProd = process.env.NODE_ENV === 'production';
const analyzerMode = process.env.REACT_APP_INTERACTIVE_ANALYZE ? 'server' : 'json';

const plugins = [];

if (isProd) {
  plugins.push(new BundleAnalyzerPlugin({ analyzerMode }));
}

module.exports = {
  webpack: {
    plugins,
  }
};
